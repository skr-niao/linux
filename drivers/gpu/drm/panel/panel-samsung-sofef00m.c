// SPDX-License-Identifier: GPL-2.0-only
/* Copyright (c) 2022 Nia Espera <a5b6@riseup.net>
 * Generated with linux-mdss-dsi-panel-driver-generator from vendor device tree:
 * Copyright (c) 2022, The Linux Foundation. All rights reserved.
 */

#include <linux/delay.h>
#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/of_device.h>
#include <linux/regulator/consumer.h>
#include <linux/swab.h>
#include <linux/backlight.h>

#include <video/mipi_display.h>

#include <drm/drm_mipi_dsi.h>
#include <drm/drm_modes.h>
#include <drm/drm_panel.h>

struct samsung_sofef00m {
	struct drm_panel panel;
	struct mipi_dsi_device *dsi;
	struct regulator *supply;
	struct gpio_desc *reset_gpio;
	const struct drm_display_mode *mode;
	bool prepared;
};

static inline
struct samsung_sofef00m *to_samsung_sofef00m(struct drm_panel *panel)
{
	return container_of(panel, struct samsung_sofef00m, panel);
}

static void samsung_sofef00m_reset(struct samsung_sofef00m *ctx)
{
	gpiod_set_value_cansleep(ctx->reset_gpio, 1);
	usleep_range(2000, 3000);
	gpiod_set_value_cansleep(ctx->reset_gpio, 0);
	usleep_range(10000, 11000);
}

static int samsung_sofef00m_on(struct samsung_sofef00m *ctx)
{
	struct mipi_dsi_device *dsi = ctx->dsi;
	struct device *dev = &dsi->dev;
	int ret;

	dsi->mode_flags |= MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_dcs_exit_sleep_mode(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to exit sleep mode: %d\n", ret);
		return ret;
	}
	usleep_range(10000, 11000);

	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xfc, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x03);
	mipi_dsi_dcs_write_seq(dsi, 0xd2, 0x9e);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	mipi_dsi_dcs_write_seq(dsi, 0xfc, 0xa5, 0xa5);
	
	usleep_range(15000, 16000);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);

	ret = mipi_dsi_dcs_set_tear_on(dsi, MIPI_DSI_DCS_TEAR_MODE_VBLANK);
	if (ret < 0) {
		dev_err(dev, "Failed to set tear on: %d\n", ret);
		return ret;
	}

	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
		ret = mipi_dsi_dcs_set_page_address(dsi, 0x0000, 0x086f);
	if (ret < 0) {
		dev_err(dev, "Failed to set page address: %d\n", ret);
		return ret;
	}
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x01);
	mipi_dsi_dcs_write_seq(dsi, 0xbb, 0x03);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x03);
	mipi_dsi_dcs_write_seq(dsi, 0xef, 0x33, 0x31, 0x14);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	mipi_dsi_dcs_write_seq(dsi, MIPI_DCS_WRITE_CONTROL_DISPLAY, 0x28);
	mipi_dsi_dcs_write_seq(dsi, MIPI_DCS_SET_DISPLAY_BRIGHTNESS, 0x00, 0x00);
	mipi_dsi_dcs_write_seq(dsi, MIPI_DCS_WRITE_POWER_SAVE, 0x00);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x05);
	mipi_dsi_dcs_write_seq(dsi, 0xb1, 0x03);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x02);
	mipi_dsi_dcs_write_seq(dsi, 0xe2, 0xb0, 0x0c, 0x04, 0x3c, 0xd3, 0x12, 0x07, 0x04, 0xae, 0x47, 0xe8, 0xcb, 0xc4, 0x11, 0xc1, 0xe9, 0xe9, 0x17, 0xff, 0xff, 0xff, 0xef, 0x11, 0x05, 0x00, 0xc4, 0x00, 0x07, 0x04, 0xb5, 0x00, 0xda, 0xc1, 0xff, 0x16, 0xc6, 0xe8, 0xe8, 0x0e, 0xff, 0xff, 0xff, 0xbb, 0x03, 0x00, 0x12, 0xdc, 0x01, 0x06, 0x04, 0xa6, 0x0d, 0xf2, 0xc8, 0xc4, 0x0a, 0xde, 0xd4, 0xed, 0x05, 0xff, 0xff, 0xff); 
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x01);
	mipi_dsi_dcs_write_seq(dsi, 0xe2, 0x01);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x02);
	mipi_dsi_dcs_write_seq(dsi, 0xd5, 0x02, 0x00, 0x14, 0x14);
	usleep_range(10000, 11000);
	ret = mipi_dsi_dcs_set_display_on(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to set display on: %d\n", ret);
		return ret;
	}
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	mipi_dsi_dcs_write_seq(dsi, 0x29, 0x00);
	mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	// ret = mipi_dsi_dcs_set_column_address(dsi, 0x0000, 0x0437);
	// if (ret < 0) {
	// 	dev_err(dev, "Failed to set column address: %d\n", ret);
	// 	return ret;
	// }



	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	// mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x09);
	// mipi_dsi_dcs_write_seq(dsi, 0xe8, 0x10, 0x30);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	// mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x07);
	// mipi_dsi_dcs_write_seq(dsi, 0xb7, 0x01);
	// mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x08);
	// mipi_dsi_dcs_write_seq(dsi, 0xb7, 0x12);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	// mipi_dsi_dcs_write_seq(dsi, 0xfc, 0x5a, 0x5a);
	// mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x01);
	// mipi_dsi_dcs_write_seq(dsi, 0xe3, 0x88);
	// mipi_dsi_dcs_write_seq(dsi, 0xb0, 0x07);
	// mipi_dsi_dcs_write_seq(dsi, 0xed, 0x67);
	// mipi_dsi_dcs_write_seq(dsi, 0xfc, 0xa5, 0xa5);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	// mipi_dsi_dcs_write_seq(dsi, MIPI_DCS_WRITE_CONTROL_DISPLAY, 0x20);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	// mipi_dsi_dcs_write_seq(dsi, MIPI_DCS_WRITE_POWER_SAVE, 0x00);
	// usleep_range(1000, 2000);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0x5a, 0x5a);
	// mipi_dsi_dcs_write_seq(dsi, 0xb3, 0x00, 0xc1);
	// mipi_dsi_dcs_write_seq(dsi, 0xf0, 0xa5, 0xa5);
	// //
	// ret = mipi_dsi_dcs_set_display_on(dsi);
	// if (ret < 0) {
	// 	dev_err(dev, "Failed to set display on: %d\n", ret);
	// 	return ret;
	// }

	// usleep_range(10000, 11000);
	// mipi_dsi_dcs_write_seq(dsi, 0x9f, 0xa5, 0xa5);
	// mipi_dsi_dcs_write_seq(dsi, 0x29);
	// mipi_dsi_dcs_write_seq(dsi, 0x9f, 0x5a, 0x5a);

	return 0;
}

static int samsung_sofef00m_off(struct samsung_sofef00m *ctx)
{
	struct mipi_dsi_device *dsi = ctx->dsi;
	struct device *dev = &dsi->dev;
	int ret;

	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_dcs_set_display_off(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to set display off: %d\n", ret);
		return ret;
	}
	usleep_range(10000, 11000);

	ret = mipi_dsi_dcs_enter_sleep_mode(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to enter sleep mode: %d\n", ret);
		return ret;
	}

	msleep(160);

	return 0;
}

static int samsung_sofef00m_prepare(struct drm_panel *panel)
{
	struct samsung_sofef00m *ctx = to_samsung_sofef00m(panel);
	struct device *dev = &ctx->dsi->dev;
	int ret;

	if (ctx->prepared)
		return 0;

	ret = regulator_enable(ctx->supply);
	if (ret < 0) {
		dev_err(dev, "Failed to enable regulator: %d\n", ret);
		return ret;
	}

	samsung_sofef00m_reset(ctx);

	ret = samsung_sofef00m_on(ctx);
	if (ret < 0) {
		dev_err(dev, "Failed to initialize panel: %d\n", ret);
		gpiod_set_value_cansleep(ctx->reset_gpio, 1);
		regulator_disable(ctx->supply);
		return ret;
	}

	ctx->prepared = true;
	return 0;
}

static int samsung_sofef00m_unprepare(struct drm_panel *panel)
{
	struct samsung_sofef00m *ctx = to_samsung_sofef00m(panel);
	struct device *dev = &ctx->dsi->dev;
	int ret;

	if (!ctx->prepared)
		return 0;

	ret = samsung_sofef00m_off(ctx);
	if (ret < 0)
		dev_err(dev, "Failed to un-initialize panel: %d\n", ret);

	gpiod_set_value_cansleep(ctx->reset_gpio, 1);
	regulator_disable(ctx->supply);

	ctx->prepared = false;
	return 0;
}

static const struct drm_display_mode samsung_sofef00m_mode = {
	.clock = (1080 + 128 + 24 + 60) * (2160 + 8 + 4 + 12) * 60 / 1000,
	.hdisplay = 1080,
	.hsync_start = 1080 + 128,
	.hsync_end = 1080 + 128 + 24,
	.htotal = 1080 + 128 + 24 + 60,
	.vdisplay = 2160,
	.vsync_start = 2160 + 8,
	.vsync_end = 2160 + 8 + 4,
	.vtotal = 2160 + 8 + 4 + 12,
	.width_mm = 68,
	.height_mm = 136,
};

static int samsung_sofef00m_get_modes(struct drm_panel *panel,
					struct drm_connector *connector)
{
	struct drm_display_mode *mode;
	struct samsung_sofef00m *ctx = to_samsung_sofef00m(panel);

	mode = drm_mode_duplicate(connector->dev, ctx->mode);
	if (!mode)
		return -ENOMEM;

	drm_mode_set_name(mode);

	mode->type = DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED;
	connector->display_info.width_mm = mode->width_mm;
	connector->display_info.height_mm = mode->height_mm;
	drm_mode_probed_add(connector, mode);

	return 1;
}

static const struct drm_panel_funcs samsung_sofef00m_panel_funcs = {
	.prepare = samsung_sofef00m_prepare,
	.unprepare = samsung_sofef00m_unprepare,
	.get_modes = samsung_sofef00m_get_modes,
};

static int sofef00m_panel_bl_update_status(struct backlight_device *bl)
{
	struct mipi_dsi_device *dsi = bl_get_data(bl);
	int err;
	u16 brightness;

	brightness = (u16)backlight_get_brightness(bl);
	// This panel needs the high and low bytes swapped for the brightness value
	brightness = __swab16(brightness);

	err = mipi_dsi_dcs_set_display_brightness(dsi, brightness);
	if (err < 0)
		return err;

	return 0;
}

static const struct backlight_ops sofef00m_panel_bl_ops = {
	.update_status = sofef00m_panel_bl_update_status,
};

static struct backlight_device *
sofef00m_create_backlight(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	const struct backlight_properties props = {
		.type = BACKLIGHT_PLATFORM,
		.brightness = 1023,
		.max_brightness = 1023,
	};

	return devm_backlight_device_register(dev, dev_name(dev), dev, dsi,
					      &sofef00m_panel_bl_ops, &props);
}

static int samsung_sofef00m_probe(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	struct samsung_sofef00m *ctx;
	int ret;

	ctx = devm_kzalloc(dev, sizeof(*ctx), GFP_KERNEL);
	if (!ctx)
		return -ENOMEM;

	ctx->mode = of_device_get_match_data(dev);

	if (!ctx->mode) {
		dev_err(dev, "Missing device mode\n");
		return -ENODEV;
	}

	ctx->supply = devm_regulator_get(dev, "vddio");
	if (IS_ERR(ctx->supply))
		return dev_err_probe(dev, PTR_ERR(ctx->supply),
				     "Failed to get vddio regulator\n");

	ctx->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(ctx->reset_gpio))
		return dev_err_probe(dev, PTR_ERR(ctx->reset_gpio),
				     "Failed to get reset-gpios\n");

	ctx->dsi = dsi;
	mipi_dsi_set_drvdata(dsi, ctx);

	dsi->lanes = 4;
	dsi->format = MIPI_DSI_FMT_RGB888;
	dsi->mode_flags = MIPI_DSI_MODE_VIDEO_BURST |
			  MIPI_DSI_MODE_NO_EOT_PACKET |
			  MIPI_DSI_CLOCK_NON_CONTINUOUS;

	drm_panel_init(&ctx->panel, dev, &samsung_sofef00m_panel_funcs,
		       DRM_MODE_CONNECTOR_DSI);

	ctx->panel.backlight = sofef00m_create_backlight(dsi);
	if (IS_ERR(ctx->panel.backlight))
		return dev_err_probe(dev, PTR_ERR(ctx->panel.backlight),
				     "Failed to create backlight\n");

	drm_panel_add(&ctx->panel);

	ret = mipi_dsi_attach(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to attach to DSI host: %d\n", ret);
		drm_panel_remove(&ctx->panel);
		return ret;
	}

	return 0;
}

static void samsung_sofef00m_remove(struct mipi_dsi_device *dsi)
{
	struct samsung_sofef00m *ctx = mipi_dsi_get_drvdata(dsi);
	int ret;

	ret = mipi_dsi_detach(dsi);
	if (ret < 0)
		dev_err(&dsi->dev, "Failed to detach from DSI host: %d\n", ret);

	drm_panel_remove(&ctx->panel);

	return;
}

static const struct of_device_id samsung_sofef00m_of_match[] = {
	{
		.compatible = "samsung,sofef00m",
		.data = &samsung_sofef00m_mode,
	},
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, samsung_sofef00m_of_match);

static struct mipi_dsi_driver samsung_sofef00m_driver = {
	.probe = samsung_sofef00m_probe,
	.remove = samsung_sofef00m_remove,
	.driver = {
		.name = "panel-samsung-sofef00m",
		.of_match_table = samsung_sofef00m_of_match,
	},
};
module_mipi_dsi_driver(samsung_sofef00m_driver);

MODULE_AUTHOR("chalkin chalkin@yeah.net");
MODULE_DESCRIPTION("DRM driver for Meizu 16th/16th plus Panel");
MODULE_LICENSE("GPL v2");
